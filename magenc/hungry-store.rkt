#lang racket

(provide hungry-store%)

;; A hungry-store wants to fill its belly with as much data as
;; possible.  If it hasn't "eaten" something, it checks its neighbors
;; for content so it can consume it as well.
;; NOTE: This only works if both stores share the same digest method.
(define hungry-store%
  (class object%
    (super-new)
    (init-field belly other-stores)
    (define/public (put! bytes)
      (send belly put! bytes))
    (define/public (get url)
      (match (send belly get url)
        [#f
         ;; Hm... we need to find someone who has this so we can eat
         ;; it...
         (call/ec
          (lambda (return)
            (for ([store other-stores])
              (define result
                (send store get url))
              (when result
                (send belly put! result)
                (return result)))
            ;; Guess not...
            #f))]
        [result result]))))

(module+ test
  (require rackunit
           "memory-store.rkt")

  (require "install-factory.rkt")
  (install-default-factories!)

  (define belly
    (new memory-store%))
  (define other-store1
    (new memory-store%))
  (define other-store2
    (new memory-store%))

  (define hungry-store
    (new hungry-store%
         [belly belly]
         [other-stores (list other-store1 other-store2)]))

  (define beep-url
    (send other-store1 put! #"beep"))
  (define boop-url
    (send other-store2 put! #"boop"))
  
  (check-false
   (send belly get beep-url))
  (check-not-false
   (send other-store1 get beep-url))
  (check-false
   (send other-store2 get beep-url))
  (check-not-false
   (send hungry-store get beep-url))
  (check-not-false
   (send belly get beep-url))
  
  (check-false
   (send belly get boop-url))
  (check-false
   (send other-store1 get boop-url))
  (check-not-false
   (send other-store2 get boop-url))
  (check-not-false
   (send hungry-store get boop-url))
  (check-not-false
   (send belly get boop-url)))
